/*
Copyright (C) 2013 Raffaele Rossi

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

Raffaele Rossi <rossi.raffaele@gmail.com> 
*/
package test.unit.com.blogspot.passionatesw.domus;

import com.blogspot.passionatesw.domus.Database;
import com.blogspot.passionatesw.domus.Logger;
import com.blogspot.passionatesw.domus.SecurityAlarm;
import com.blogspot.passionatesw.domus.Sensor;
import com.blogspot.passionatesw.domus.Siren;

import junit.framework.Assert;
import junit.framework.TestCase; 

public class SecurityAlarmTest extends TestCase {

  class StubDatabase implements Database {
    @Override
    public String getPin() { return "0000"; }
    @Override
    public void setPin(String pin) {
      /* Do nothing */
    }
  }

  class FakeDatabase implements Database {
    String pin = "0000";
    @Override
    public String getPin() { return pin; }
    @Override
    public void setPin(String pin) { this.pin = pin; }
  }

  class DummyLogger implements Logger {
    @Override public void alarmEngaged() { }
    @Override public void alarmDisengaged() { }
  }

  public void testAlarmNotEngagedAtStartup() {
    // setup
    SecurityAlarm alarm = new SecurityAlarm(null, null);
    // verify
    assertFalse("Alarm engaged", alarm.isEngaged());
  }


  public void testEngageAlarm() {
    // setup
    StubDatabase db = new StubDatabase();
    SecurityAlarm alarm = new SecurityAlarm(db, new DummyLogger());
    // exercise
    alarm.engage("0000");
    // verify
    assertTrue("Alarm not engaged", alarm.isEngaged());
  }

  public void testDisengageAlarm() { 
    // setup
    StubDatabase db = new StubDatabase();
    SecurityAlarm alarm = new SecurityAlarm(db, new DummyLogger());
    alarm.engage("0000");
    // exercise
    alarm.disengage("0000");
    // verify
    assertFalse("Alarm engaged", alarm.isEngaged());
  }

  public void testDoNotEngageAlarmIfPinIsWrong() {
    // setup
    StubDatabase db = new StubDatabase();
    SecurityAlarm alarm = new SecurityAlarm(db, new DummyLogger());
    // exercise
    alarm.engage("1234");
    // verify
    assertFalse("Alarm engaged", alarm.isEngaged());
  }

  public void testDoNotDisengageAlarmIfPinIsWrong() {
    // setup
    StubDatabase db = new StubDatabase();
    SecurityAlarm alarm = new SecurityAlarm(db, new DummyLogger());
    alarm.engage("0000");
    // exercise
    alarm.disengage("1234");
    // verify
    assertTrue("Alarm not engaged", alarm.isEngaged());
  }

  public void testChangePinCode() { 
    // setup
    FakeDatabase db = new FakeDatabase();
    SecurityAlarm alarm = new SecurityAlarm(db, new DummyLogger());
    // exercise
    alarm.changePinCode("0000", "1234");
    alarm.engage("1234");
    // verify
    assertTrue("Alarm not engaged", alarm.isEngaged());
  }

  public void testDoNotChangePinCodeIfOldOneIsWrong() { 
    // setup
    StubDatabase db = new StubDatabase();
    SecurityAlarm alarm = new SecurityAlarm(db, null);
    // exercise
    alarm.changePinCode("1234", "5678");
    alarm.engage("5678");
    // verify
    assertFalse("Alarm engaged", alarm.isEngaged());
  }

  public void testCanChangePinCodeMoreThanOnce() { 
    // setup
    FakeDatabase db = new FakeDatabase();
    SecurityAlarm alarm = new SecurityAlarm(db, new DummyLogger());
    // exercise
    alarm.changePinCode("0000", "1234");
    alarm.changePinCode("1234", "5678");
    alarm.engage("5678");
    // verify
    assertTrue("Alarm not engaged", alarm.isEngaged());
  }

  class SpySiren implements Siren {
    private boolean _activated = false;
    @Override
    public void activate() {
      _activated = true;
    }

    public void assertActivated() {
      assertTrue("Siren not activated", _activated);
    }

    public void assertNotActivated() {
      assertFalse("Siren activated", _activated);
    }
  }

  public void testActivateTheSirenWhenTheSensorTriggers() {
    // setup
    StubDatabase db = new StubDatabase();
    SecurityAlarm alarm = new SecurityAlarm(db, new DummyLogger());
    SpySiren spySiren = new SpySiren();
    Sensor sensor = new Sensor();
    alarm.addSiren(spySiren);
    alarm.addSensor(sensor);
    alarm.engage("0000");
    // exercise
    sensor.trigger();
    // verify
    spySiren.assertActivated();
  }

  public void testDoNotActivateTheSirenIfNotEngaged() {
    // setup
    SecurityAlarm alarm = new SecurityAlarm(null, null);
    SpySiren spySiren = new SpySiren();
    Sensor sensor = new Sensor();
    alarm.addSiren(spySiren);
    alarm.addSensor(sensor);
    // exercise
    sensor.trigger();
    // verify
    spySiren.assertNotActivated();
  }

  public void testActivateTheSirenWhenFirstSensorTriggers() {
    // setup
    StubDatabase db = new StubDatabase();
    SecurityAlarm alarm = new SecurityAlarm(db, new DummyLogger());
    SpySiren spySiren = new SpySiren();
    Sensor sensor1 = new Sensor();
    Sensor sensor2 = new Sensor();
    alarm.addSiren(spySiren);
    alarm.addSensor(sensor1);
    alarm.addSensor(sensor2);
    alarm.engage("0000");
    // exercise
    sensor1.trigger();
    // verify
    spySiren.assertActivated();
  }

  public void testActivateTheSirenWhenSecondSensorTriggers() {
    // setup
    StubDatabase db = new StubDatabase();
    SecurityAlarm alarm = new SecurityAlarm(db, new DummyLogger());
    SpySiren spySiren = new SpySiren();
    Sensor sensor1 = new Sensor();
    Sensor sensor2 = new Sensor();
    alarm.addSiren(spySiren);
    alarm.addSensor(sensor1);
    alarm.addSensor(sensor2);
    alarm.engage("0000");
    // exercise
    sensor2.trigger();
    // verify
    spySiren.assertActivated();
  }

  public void testActivateAllSirensWhenSensorsTrigger() {
    // setup
    StubDatabase db = new StubDatabase();
    SecurityAlarm alarm = new SecurityAlarm(db, new DummyLogger());
    SpySiren spySiren1 = new SpySiren();
    SpySiren spySiren2 = new SpySiren();
    Sensor sensor = new Sensor();
    alarm.addSiren(spySiren1);
    alarm.addSiren(spySiren2);
    alarm.addSensor(sensor);
    alarm.engage("0000");
    // exercise
    sensor.trigger();
    // verify
    spySiren1.assertActivated();
    spySiren2.assertActivated();
  }

  class SpyLogger implements Logger {
    static final String ALARM_ENGAGED = "Alarm engaged";
    static final String ALARM_DISENGAGED = "Alarm disengaged";
    private String _log;

    @Override public void alarmEngaged() { _log = ALARM_ENGAGED; }
    @Override public void alarmDisengaged() { _log = ALARM_DISENGAGED; }

    public void assertLogged(String log) {
      Assert.assertEquals("Log", log, _log);
    }

  }

  public void testLogWhenAlarmIsEngaged() {
    // setup
    StubDatabase db = new StubDatabase();
    SpyLogger logger = new SpyLogger();
    SecurityAlarm alarm = new SecurityAlarm(db, logger);
    // exercise
    alarm.engage("0000");
    // verify
    logger.assertLogged(SpyLogger.ALARM_ENGAGED);
  }

  public void testLogWhenAlarmIsDisengaged() {
    // setup
    StubDatabase db = new StubDatabase();
    SpyLogger logger = new SpyLogger();
    SecurityAlarm alarm = new SecurityAlarm(db, logger);
    // exercise
    alarm.engage("0000");
    alarm.disengage("0000");
    // verify
    logger.assertLogged(SpyLogger.ALARM_DISENGAGED);
  }
}
