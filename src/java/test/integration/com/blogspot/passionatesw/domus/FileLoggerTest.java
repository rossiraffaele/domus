/*
  Copyright (C) 2013 Raffaele Rossi

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

  Raffaele Rossi <rossi.raffaele@gmail.com>
*/
package test.integration.com.blogspot.passionatesw.domus;

import java.io.File;
import java.util.Scanner;
import junit.framework.TestCase;

import com.blogspot.passionatesw.domus.FileLogger;

public class FileLoggerTest extends TestCase {
  
  static private final String LOG_FILE = "log.txt";

  protected void tearDown() { 
    File log = new File(LOG_FILE);
    assertTrue("Can't delete log file", log.delete());
  }

  public void assertLogLine(String expectedLog) throws Exception {
    Scanner log = new Scanner(new File(LOG_FILE));
    assertEquals("Log line", expectedLog, log.useDelimiter("\\n").next());
  }

  public void testWriteAlarmEngagedLog() throws Exception {
    // setup
    FileLogger logger = new FileLogger(LOG_FILE);
    // exercise
    logger.alarmEngaged();
    // verify
    assertLogLine("Alarm engaged");
  }

  public void testWriteAlarmDisengagedLog() throws Exception {
    // setup
    FileLogger logger = new FileLogger(LOG_FILE);
    // exercise
    logger.alarmDisengaged();
    // verify
    assertLogLine("Alarm disengaged");
  }

}
