/*
  Copyright (C) 2013 Raffaele Rossi

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

  Raffaele Rossi <rossi.raffaele@gmail.com>
*/
package test.integration.com.blogspot.passionatesw.domus;
import java.io.File;
import java.util.Scanner;

import junit.framework.TestCase;

import com.blogspot.passionatesw.domus.FileSiren;

public class FileSirenTest extends TestCase {
  
  public void testWriteOneWhenActivated() throws Exception {
    // setup
    FileSiren siren = new FileSiren("siren.out");
    // exercise
    siren.activate();
    // verify
    File sirenFile = new File("siren.out");
    Scanner sirenScanner = new Scanner(sirenFile);
    assertEquals("Siren output", 1, sirenScanner.nextInt());
    sirenFile.delete();
  }

  public void testWriteZeroAfterCreation() throws Exception {
    // setup
    FileSiren siren = new FileSiren("siren.out");
    // verify
    File sirenFile = new File("siren.out");
    Scanner sirenScanner = new Scanner(sirenFile);
    assertEquals("Siren output", 0, sirenScanner.nextInt());
    sirenFile.delete();
  }

}
