/*
  Copyright (C) 2013 Raffaele Rossi

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

  Raffaele Rossi <rossi.raffaele@gmail.com>
*/
package test.integration.com.blogspot.passionatesw.domus;

import java.io.File;
import java.io.FileWriter;
import java.util.Scanner;

import com.blogspot.passionatesw.domus.FileDatabase;

import junit.framework.TestCase;

public class FileDatabaseTest extends TestCase {
  
  static private final String PIN_FILE = "pin.dat";
  static private final String FACTORY_PIN = "0000";

  protected void tearDown() {
    File file = new File(PIN_FILE);
    assertTrue("File couldn't be deleted", file.delete());
  }

  public void assertPinCode(String expectedPin) throws Exception {
    Scanner dbScanner = new Scanner(new File(PIN_FILE));
    assertEquals("PIN code on file", expectedPin, dbScanner.next());
    dbScanner.close();
  }

  public void testCreatePinFileIfNotExists() throws Exception {
    // setup
    FileDatabase db = new FileDatabase(PIN_FILE);
    // verify
    assertPinCode(FACTORY_PIN);
  }

  public void testRetrivePinCode() throws Exception {
    // setup
    FileDatabase db = new FileDatabase(PIN_FILE);
    // verify
    assertEquals("PIN code", FACTORY_PIN, db.getPin());
  }

  public void testRetrivePersistedPinCode() throws Exception { 
    // setup
    FileWriter file = new FileWriter(PIN_FILE);
    file.write("1234");
    file.close();
    FileDatabase db = new FileDatabase(PIN_FILE);
    // verify
    assertEquals("PIN code", "1234", db.getPin());
  }

  public void testPersistPinCode() throws Exception { 
    // setup
    FileDatabase db = new FileDatabase(PIN_FILE);
    // exercise
    db.setPin("1234");
    // verify
    assertPinCode("1234");
  }

}
