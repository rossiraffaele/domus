/*
  Copyright (C) 2013 Raffaele Rossi

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

  Raffaele Rossi <rossi.raffaele@gmail.com>
*/
package test.component.com.blogspot.passionatesw.domus;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.util.Scanner;

import com.blogspot.passionatesw.domus.Main;

import junit.framework.TestCase;

public class DomusTest extends TestCase {
  
  static private final String PIN_FILE = "pin.dat";
  static private final String LOG_FILE = "log.txt";
  static private final String SIREN_FILE = "siren.out";

  protected void tearDown() {
    delete(PIN_FILE);
    delete(LOG_FILE);
    delete(SIREN_FILE);
  }
  
  private void delete(String file) {
    assertTrue("Can't delete " + file, new File(file).delete());
  }

  private void assertSirenActivated(boolean activated) throws Exception {
    Scanner sirenScanner = new Scanner(new File(SIREN_FILE));
    assertEquals("Siren not activated",
                 activated ? 1 : 0,
                 sirenScanner.nextInt());
  }

  public void testActivateSiren() throws Exception {
    // setup
    StringBuffer commands = new StringBuffer();
    commands.append("1\n"); /* Add a sensor */
    commands.append("2\n"); /* Engage the alarm */
    commands.append("0000\n");
    commands.append("4\n"); /* Trigger a sensor */
    commands.append("0\n");
    commands.append("0\n"); /* Quit the program */
    System.setIn(new ByteArrayInputStream(commands.toString().getBytes()));
    // exercise
    Main.main(new String[] {PIN_FILE, LOG_FILE, SIREN_FILE});
    // verify
    assertSirenActivated(true);
  }

  public void testChangePinCode() throws Exception {
    // setup
    StringBuffer commands = new StringBuffer();
    commands.append("5\n"); /* Change PIN code */
    commands.append("0000\n");
    commands.append("1234\n");
    commands.append("1\n"); /* Add a sensor */
    commands.append("2\n"); /* Engage the alarm */
    commands.append("1234\n");
    commands.append("4\n"); /* Trigger a sensor */
    commands.append("0\n");
    commands.append("0\n"); /* Quit the program */
    System.setIn(new ByteArrayInputStream(commands.toString().getBytes()));
    // exercise
    Main.main(new String[] {PIN_FILE, LOG_FILE, SIREN_FILE});
    // verify
    assertSirenActivated(true);
  }

  public void testDisengageAlarm() throws Exception {
    // setup
    StringBuffer commands = new StringBuffer();
    commands.append("1\n"); /* Add a sensor */
    commands.append("2\n"); /* Engage the alarm */
    commands.append("0000\n");
    commands.append("3\n"); /* Engage the alarm */
    commands.append("0000\n");
    commands.append("4\n"); /* Trigger a sensor */
    commands.append("0\n");
    commands.append("0\n"); /* Quit the program */
    System.setIn(new ByteArrayInputStream(commands.toString().getBytes()));
    // exercise
    Main.main(new String[] {PIN_FILE, LOG_FILE, SIREN_FILE});
    // verify
    assertSirenActivated(false);
  }
}
