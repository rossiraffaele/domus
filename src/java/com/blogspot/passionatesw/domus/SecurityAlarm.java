/*
Copyright (C) 2013 Raffaele Rossi

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

Raffaele Rossi <rossi.raffaele@gmail.com> 
*/
package com.blogspot.passionatesw.domus;
import java.util.ArrayList;

import com.blogspot.passionatesw.domus.Sensor.TriggerListener;

public class SecurityAlarm implements Sensor.TriggerListener {

  private boolean _engaged = false;
  private Database _db;
  private Logger _logger;
  private ArrayList<Siren> _sirens = new ArrayList<Siren>();

  public SecurityAlarm(Database db, Logger logger) {
    _db = db;
    _logger = logger;
  }

  private boolean isPinValid(String pin) {
    return pin.equals(_db.getPin());
  }

  public boolean isEngaged() {
    return _engaged;
  }

  public void engage(String pin) {
    if (isPinValid(pin)) {
      _engaged = true;
      _logger.alarmEngaged();
    }
  }

  public void disengage(String pin) {
    if (isPinValid(pin)) {
      _engaged = false;
      _logger.alarmDisengaged();
    }
  }

  public void changePinCode(String oldPin, String newPin) {
    if (isPinValid(oldPin)) {
      _db.setPin(newPin);
    }
  }

  @Override
  public void triggered() {
    if (isEngaged()) {
      for (Siren s: _sirens) {
        s.activate();
      }
    }
  }

  public void addSensor(Sensor sensor) {
    sensor.addTriggerListener(this);
  }

  public void addSiren(Siren siren) {
    _sirens.add(siren);
  }
}
