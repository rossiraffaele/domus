/*
  Copyright (C) 2013 Raffaele Rossi

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

  Raffaele Rossi <rossi.raffaele@gmail.com>
*/
package com.blogspot.passionatesw.domus;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class Main {
  
  static public void main(String[] args) throws Exception {
    new Main(args[0], args[1], args[2]).start();
  }

  private SecurityAlarm _alarm;
  private ArrayList<Command> _commands;
  private boolean _run;
  private Scanner _input;
  private ArrayList<Sensor> _sensors;

  private interface Command extends Runnable {
    String getDescription();
  }

  public Main(String dbFileName, String loggerFileName, String sirenFileName)
  throws Exception {
    _alarm = new SecurityAlarm(new FileDatabase(dbFileName),
                               new FileLogger(loggerFileName));
    _alarm.addSiren(new FileSiren(sirenFileName));
    _input = new Scanner(System.in);
    _sensors = new ArrayList<Sensor>();
    _commands = new ArrayList<Command>();
    _commands.add(new Command() {
        @Override
        public String getDescription() {
          return "Exit probgram";
        }

        @Override
        public void run() {
          _run = false;
        }
      });

    _commands.add(new Command() {
        @Override
        public String getDescription() {
          return "Add sensor";
        }

        @Override
        public void run() {
          Sensor s = new Sensor();
          _sensors.add(s);
          _alarm.addSensor(s);
        }
      });

    _commands.add(new Command() {
        @Override
        public String getDescription() {
          return "Engage alarm";
        }

        @Override
        public void run() {
          System.out.println("Insert PIN code");
          String pin = _input.next();
          _alarm.engage(pin);
        }
      });

    _commands.add(new Command() {
        @Override
        public String getDescription() {
          return "Disengage alarm";
        }

        @Override
        public void run() {
          System.out.println("Insert PIN code");
          String pin = _input.next();
          _alarm.disengage(pin);
        }
      });

    _commands.add(new Command() {
        @Override
        public String getDescription() {
          return "Trigger a sensor";
        }

        @Override
        public void run() {
          System.out.println("Select a sensor");
          int i = 0;
          for (Sensor s: _sensors) {
            System.out.println(i++);
          }
          int sensorId = _input.nextInt();
          _sensors.get(sensorId).trigger(); 
        }
      });

    _commands.add(new Command() {
        @Override
        public String getDescription() {
          return "Change PIN code";
        }

        @Override
        public void run() {
          System.out.println("Insert old PIN code");
          String oldPin = _input.next();
          System.out.println("Insert new PIN code");
          String newPin = _input.next();
          _alarm.changePinCode(oldPin, newPin);
        }
      });
  }

  private void printMenu() {
    System.out.println("Select a command");
    for (int i = 0; i < _commands.size(); i++) {
      Command c = _commands.get(i);
      System.out.println(String.format("%d - %s", i, c.getDescription()));
    }
  }

  private void start() {
    _run = true;
    while (_run) {
      printMenu();
      int choice = _input.nextInt();
      _commands.get(choice).run();
    }
  }

}
