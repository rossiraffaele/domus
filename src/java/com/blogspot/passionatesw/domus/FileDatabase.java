/*
  Copyright (C) 2013 Raffaele Rossi

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

  Raffaele Rossi <rossi.raffaele@gmail.com>
*/
package com.blogspot.passionatesw.domus;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class FileDatabase implements Database {

  private String _pinFileName;

  public FileDatabase(String pinFileName) throws IOException {
    _pinFileName = pinFileName;
    File pinFile = new File(_pinFileName);
    if ( ! pinFile.exists()) {
      FileWriter pinFileWriter = new FileWriter(pinFile);
      pinFileWriter.write("0000");
      pinFileWriter.close();
    }
  }

  @Override
  public String getPin() {
    try { 
      Scanner pinFileScanner = new Scanner(new File(_pinFileName)); 
      String pin = pinFileScanner.next(); 
      pinFileScanner.close(); 
      return pin;
    } catch (Exception e) {
      throw new RuntimeException("Can't retrieve PIN code", e);
    }
  } 

  @Override
  public void setPin(String pin) {
    try { 
      FileWriter pinFileWriter = new FileWriter(_pinFileName); 
      pinFileWriter.write(pin); 
      pinFileWriter.close();
    } catch (Exception e) {
      throw new RuntimeException("Can't persist PIN code", e);
    }
  }

}
